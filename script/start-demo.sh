#!/usr/bin/env bash

scriptPath=$(dirname "$(readlink -f "$0")")

${scriptPath}/start-container.sh -container "as.container" -port 1234 &

${scriptPath}/start-launcher.sh -remotePeer "localhost:1234" -config "res/frontend-demo.xml"