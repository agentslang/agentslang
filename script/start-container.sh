#!/usr/bin/env bash

scriptPath=$(dirname "$(readlink -f "$0")")

source "${scriptPath}/include.sh"
initPaths

javaJVM='java'
javaOptions="-Dlog4j.configurationFile=${resPath}/log4j2-console.xml"
javaLibraryPath="-Djava.library.path=${libraryPath}"
alvariumContainerRunner='org.alvarium.container.ContainerDaemon'

getDependencies
alvariumLibraries=${returnDependencies}

if [ -z ${alvariumLibraries} ]; then
    echo "No libraries found in Alvarium libraries path. Exiting ..."
    exit 1
else
    echo "JVM: ${javaJVM}"
    echo "Alvarium java options: ${javaOptions}"
    echo "Alvarium libraries: ${alvariumLibraries}"
    echo "Alvarium runner: ${alvariumContainerRunner}"
    echo "Alvarium parameters: $@"

    ${javaJVM} ${javaLibraryPath} ${javaOptions} -cp ${alvariumLibraries} ${alvariumContainerRunner} "$@"
fi
