#!/usr/bin/env bash

alvariumLibFolder="lib"

getDependencies() {
    returnDependencies=$(find ${libraryPath} -type f -name "*.jar" | tr '\n' ':')
    returnDependencies=${returnDependencies::-1}
}

setupProductionPath() {
    # Production path setup
    prodPath='./'
    if [ ! -e "${prodPath}${alvariumLibFolder}" ]; then
        if [ -e "../${prodPath}${alvariumLibFolder}" ]; then
            # the alternative production path exists
            prodPath="../${prodPath}"
        else
            echo "The production path does not exist: ${prodPath}. Please run ant release first ..."
            exit 1
        fi
    fi
}

setupLibraryPath() {
    # Libraries setup
    libraryPath="${prodPath}lib/"
    if [ ! -e ${libraryPath} ]; then
        echo "The library path does not exist: ${libraryPath}"
        exit 1
    fi
}

setupResPath() {
    # Resources path setup
    resPath="${prodPath}res/"
    if [ ! -e ${resPath} ]; then
        echo "The res path does not exist: ${resPath}"
        exit 1
    fi
}

initPaths() {
    setupProductionPath
    setupLibraryPath
    setupResPath
}