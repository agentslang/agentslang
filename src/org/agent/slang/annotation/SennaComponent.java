/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.annotation;

import org.agent.slang.data.annotation.GenericTextAnnotation;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.utils.FileUtils;
import org.syn.n.bad.annotation.AnnotationToken;
import org.syn.n.bad.annotation.TextAnnotationConstants;
import org.syn.n.bad.annotation.TextToken;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static org.agent.slang.annotation.ExternalProcessor.processText;
import static org.agent.slang.util.PropertyUtils.getFilename;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 12/9/12
 */
@ConfigureParams(mandatoryConfigurationParams = {"sennaPath"}, optionalConfigurationParams = {"sennaParams"},
        outputChannels = "senna.data", outputDataTypes = GenericTextAnnotation.class,
        inputDataTypes = StringData.class)
public class SennaComponent extends MixedComponent {
    private static final String SENNA_PATH = "sennaPath";
    private static final String SENNA_PARAMS = "sennaParams";
    private static final String SENNA_OUT = "senna.data";

    private static final Map<String, String> sennaParamMappings = new HashMap<>();
    private static final Map<String, Integer> sennaParamIndexMappings = new HashMap<>();

    static {
        sennaParamMappings.put("-pos", TextAnnotationConstants.POS);
        sennaParamMappings.put("-chk", TextAnnotationConstants.CHK);
        sennaParamMappings.put("-ner", TextAnnotationConstants.NER);
//        sennaParamMappings.put("-srl", TextAnnotationConstants.SRL);
//        sennaParamMappings.put("-psg", TextAnnotationConstants.PSG);

        sennaParamIndexMappings.put("-pos", 0);
        sennaParamIndexMappings.put("-chk", 1);
        sennaParamIndexMappings.put("-ner", 2);
//        sennaParamIndexMappings.put("-srl", 3);
//        sennaParamIndexMappings.put("-psg", 4);
    }

    private static Map<Integer, String> paramMappings;

    private File sennaPath;
    private String sennaCommand;

    public SennaComponent(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        paramMappings = new HashMap<>();

        sennaPath = getFilename(listProperties.getProperty(SENNA_PATH));
        FileUtils.checkExecutableFile(sennaPath, true);

        sennaCommand = sennaPath.getAbsolutePath();
        sennaCommand = sennaCommand + " " + initParameterMappings(listProperties.getProperty(SENNA_PARAMS));
    }

    protected void handleData(GenericData data) {
        publishData(SENNA_OUT, processText(data.getId(), ((StringData) data).getSessionId(),
                                           ((StringData) data).getData(), sennaPath, sennaCommand,
                                           SennaComponent::processLine, line -> false));
    }

    private static void processLine(GenericTextAnnotation result, String line, int lineNumber) {
        line = line.trim();
        String[] items = line.split("\t");
        int index = -1;
        for (int i = 0; i < items.length; i++) {
            if (i == 0) {
                //text token
                if (items[i].trim().length() > 0) {
                    TextToken token = new TextToken(items[i].trim());
                    index = result.addTextToken(token);
                } else {
                    break;
                }
            } else {
                int level = TextAnnotationConstants.getLevel(paramMappings.get(i));
                byte label = TextAnnotationConstants.transformAnnotationLabel(level, items[i].trim());
                if (label >= 0) {
                    result.addAnnotationToken(level, new AnnotationToken(index, label));
                }
            }
        }
    }

    private static String initParameterMappings(String sennaParametersString) {
        if (sennaParametersString != null) {
            sennaParametersString = sennaParametersString.trim();
            if (sennaParametersString.contains(",")) {
                sennaParametersString = sennaParametersString.replace(",", " ");
            }
            String[] params = sennaParametersString.split(" ");

            String[] orderedParams = new String[sennaParamIndexMappings.size()];
            for (String param : params) {
                param = param.trim();
                Integer index = sennaParamIndexMappings.get(param);
                String textAnnotationParam = sennaParamMappings.get(param);
                if (textAnnotationParam != null && index != null && index >= 0) {
                    orderedParams[index] = sennaParamMappings.get(param);
                } else {
                    throw new IllegalArgumentException("Invalid senna option:" + param);
                }
            }

            int index = 1;
            for (String param : orderedParams) {
                if (param != null) {
                    paramMappings.put(index, param);
                    index++;
                }
            }

            return sennaParametersString;
        } else {
            return "";
        }
    }
}
