/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.annotation;

import org.agent.slang.data.annotation.GenericTextAnnotation;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.utils.FileUtils;
import org.syn.n.bad.annotation.AnnotationToken;
import org.syn.n.bad.annotation.TextAnnotationConstants;
import org.syn.n.bad.annotation.TextToken;

import java.io.File;

import static org.agent.slang.annotation.ExternalProcessor.processText;
import static org.agent.slang.util.PropertyUtils.getFilename;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 30/10/13
 */

@ConfigureParams(mandatoryConfigurationParams = {"meltPath"}, optionalConfigurationParams = {"meltParams"},
        outputChannels = "melt.data", outputDataTypes = GenericTextAnnotation.class,
        inputDataTypes = StringData.class)
public class MELtComponent extends MixedComponent {
    private static final String MELT_PATH = "meltPath";
    private static final String MELT_PARAMS = "meltParams";
    private static final String MELT_OUT = "melt.data";
    private File meltPath;
    private String meltCommand;

    public MELtComponent(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        meltPath = getFilename(listProperties.getProperty(MELT_PATH));
        FileUtils.checkReadableFile(meltPath, true);

        meltCommand = meltPath.getAbsolutePath();
        meltCommand = meltCommand + " " + listProperties.getProperty(MELT_PARAMS, "");
    }

    protected void handleData(GenericData data) {
        publishData(MELT_OUT, processText(data.getId(), ((StringData) data).getSessionId(),
                                          ((StringData) data).getData(), meltPath, meltCommand,
                                          MELtComponent::processLine, MELtComponent::canIgnoreErrorLine));
    }

    private static void processLine(GenericTextAnnotation result, String line, int lineNumber) {
        String[] items = line.split(" ");

        for (String item : items) {
            String[] parts = item.split("/");
            TextToken token = new TextToken(parts[0].trim());
            int index = result.addTextToken(token);

            int level = TextAnnotationConstants.getLevel(TextAnnotationConstants.POS);
            byte label = TextAnnotationConstants.transformAnnotationLabel(level, parts[1].trim());
            if (label >= 0) {
                result.addAnnotationToken(level, new AnnotationToken(index, label));
            }
        }
    }

    private static boolean canIgnoreErrorLine(String line) {
        return line.contains("Loading") || line.contains("done") || line.contains("POS Tagging...");
    }
}
