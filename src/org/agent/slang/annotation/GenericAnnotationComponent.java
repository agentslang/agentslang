/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.annotation;

import org.agent.slang.data.annotation.GenericTextAnnotation;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.utils.FileUtils;
import org.syn.n.bad.annotation.AnnotationToken;
import org.syn.n.bad.annotation.TextAnnotationConstants;
import org.syn.n.bad.annotation.TextToken;

import java.io.File;

import static org.agent.slang.annotation.ExternalProcessor.processText;
import static org.agent.slang.util.PropertyUtils.getFilename;
import static org.alvarium.logger.Logger.Level.DEBUG;
import static org.alvarium.logger.Logger.log;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 30/10/13
 *          <p>
 *          This works well for Tree Tagger and Morfette or any tool that outputs
 *          [token] [lemma] [POS] on each line
 *          This should be very similar to the GOLD standard output
 *          <p>
 *          If used on other toolkit the component may report errors because the tool may print some debug lines on std error
 */

@ConfigureParams(mandatoryConfigurationParams = {"toolPath"}, optionalConfigurationParams = {"toolParams"},
        outputChannels = "generic.annotation.data", outputDataTypes = GenericTextAnnotation.class,
        inputDataTypes = StringData.class)
public class GenericAnnotationComponent extends MixedComponent {
    private static final String TOOL_PATH = "toolPath";
    private static final String TOOL_PARAMS = "toolParams";

    private static final String ANNOTATION_OUT = "generic.annotation.data";

    private static final int ANNOTATION_IDX_TOKEN = 0;
    private static final int ANNOTATION_IDX_LEMMA = 1;
    private static final int ANNOTATION_IDX_POS = 2;
    private static final int ANNOTATION_IDX_LENGTH = 3;

    private File toolPath;
    private String toolCommand;

    public GenericAnnotationComponent(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        toolPath = getFilename(listProperties.getProperty(TOOL_PATH));
        FileUtils.checkReadableFile(toolPath, true);

        toolCommand = toolPath.getAbsolutePath() + " " + listProperties.getProperty(TOOL_PARAMS, "");
    }

    protected void handleData(GenericData data) {
        publishData(ANNOTATION_OUT, processText(data.getId(), ((StringData) data).getSessionId(),
                                                ((StringData) data).getData(),
                                                toolPath, toolCommand,
                                                GenericAnnotationComponent::processLine,
                                                GenericAnnotationComponent::canIgnoreErrorLine));
    }


    private static void processLine(GenericTextAnnotation result, String line, int lineNumber) {
        String[] items = line.split(" ");
        if (items.length != ANNOTATION_IDX_LENGTH) {
            log(GenericAnnotationComponent.class, DEBUG, "Something went wrong. The line has " + items.length + " tokens. Line = " + line);
        } else {
            TextToken token = new TextToken(items[ANNOTATION_IDX_TOKEN].trim());
            int index = result.addTextToken(token);

            int level = TextAnnotationConstants.getLevel(TextAnnotationConstants.POS);
            byte label = TextAnnotationConstants.transformAnnotationLabel(level, items[ANNOTATION_IDX_POS].trim());
            if (label >= 0) {
                result.addAnnotationToken(level, new AnnotationToken(index, label));
            }

            if (items[ANNOTATION_IDX_LEMMA] != null && items[ANNOTATION_IDX_LEMMA].trim().length() > 0
                    && !items[ANNOTATION_IDX_LEMMA].trim().equals("<unknown>")) {
                result.setLemma(index, new TextToken(items[ANNOTATION_IDX_LEMMA].trim()));
            }
        }
    }

    private static boolean canIgnoreErrorLine(String line) {
        return line.contains("Loading models from") || line.contains("reading parameters")
                || line.contains("tagging") || line.contains("finished");
    }
}
