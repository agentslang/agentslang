package org.agent.slang.annotation;

import org.agent.slang.data.annotation.GenericTextAnnotation;
import org.syn.n.bad.annotation.Annotation;
import org.syn.n.bad.annotation.TextAnnotationConstants;

import java.io.*;

import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.log;

class ExternalProcessor {
    interface LineProcessorDelegate {
        void processLine(GenericTextAnnotation result, String line, int lineNumber);
    }

    interface ErrorIgnoreDelegate {
        boolean canIgnoreErrorLine(String line);
    }

    static GenericTextAnnotation processText(String id, String sessionId, String data, File toolPath, String toolName,
                                             LineProcessorDelegate lineProcessorDelegate,
                                             ErrorIgnoreDelegate errorIgnoreDelegate) {
        GenericTextAnnotation result = new GenericTextAnnotation(id, sessionId, estimateSize(data));
        result.addAnnotation(TextAnnotationConstants.getLevel(TextAnnotationConstants.POS), new Annotation());

        try {
            Process process = Runtime.getRuntime().exec(toolName, new String[0], toolPath.getParentFile());

            PrintWriter pw = new PrintWriter(process.getOutputStream());
            pw.println(data);
            pw.flush();
            pw.close();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            int lineCount = 0;
            String line;
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                if (line.length() > 0) {
                    lineProcessorDelegate.processLine(result, line, lineCount);
                }
            }
            reader.close();

            BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            StringBuilder sb = new StringBuilder();
            while ((line = errorReader.readLine()) != null) {
                if (!errorIgnoreDelegate.canIgnoreErrorLine(line)) {
                    sb.append(line).append("\n");
                }
            }
            errorReader.close();
            if (sb.length() > 0) {
                log(ExternalProcessor.class, CRITICAL, "Tool = %s ToolPath = %s Error = %s",
                    toolName, toolPath, sb.toString());
            }

            process.destroy();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private static int estimateSize(String data) {
        int count = 0;
        int index = -1;
        while ((index = data.indexOf(' ', index + 1)) != -1) {
            count++;
        }
        return count;
    }
}
