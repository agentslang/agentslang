/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.in.google;


import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 9/25/13
 */
public class GoogleASRRequest extends Thread {
    private final List<String> flacPaths = new LinkedList<String>();
    private boolean running = false;

    private final List<TranscriptionListener> transcriptionListeners = new LinkedList<TranscriptionListener>();

    private Locale transcriptionLanguage;
    private int moreOptionsSize = 10;
    private boolean profanityFilter = false;

    public GoogleASRRequest(Locale transcriptionLanguage, int moreOptionsSize, boolean profanityFilter) {
        this.transcriptionLanguage = transcriptionLanguage;
        this.moreOptionsSize = moreOptionsSize;
        this.profanityFilter = profanityFilter;
    }

    public void pushTranscription(String flacPath) {
        synchronized (flacPaths) {
            flacPaths.add(flacPath);
            flacPaths.notify();
        }
    }

    public synchronized void start() {
        running = true;
        super.start();
    }

    public void stopThread() {
        running = false;
    }

    public void run() {
        while (running) {
            String item = null;
            synchronized (flacPaths) {
                if (flacPaths.isEmpty()) {
                    try {
                        flacPaths.wait();
                    } catch (InterruptedException e) {
                        running = false;
                        //-- sleep
                    }
                } else {
                    item = flacPaths.remove(0);
                }
            }
            if (item != null) {
                transcribeFlac(item);
            }
        }
    }

    public void addTranscriptionLister(TranscriptionListener listener) {
        synchronized (transcriptionListeners) {
            transcriptionListeners.add(listener);
        }
    }

    public void removeTranscriptionLister(TranscriptionListener listener) {
        synchronized (transcriptionListeners) {
            transcriptionListeners.remove(listener);
        }
    }

    private void fireTranscriptionEvent(String transcription, double confidence, Locale language,
                                        List<String> moreOptions) {
        synchronized (transcriptionListeners) {
            for (TranscriptionListener listener : transcriptionListeners) {
                listener.transcriptionReceived(transcription, confidence, language, moreOptions);
            }
        }
    }

    private void fireTranscriptionFailed() {
        synchronized (transcriptionListeners) {
            for (TranscriptionListener listener : transcriptionListeners) {
                listener.transcriptionFailed();
            }
        }
    }

    private void transcribeFlac(String flacPath) {
        try {
            VoiceUtility.Response response = VoiceUtility
                    .transcribeFromFLAC(new File(flacPath), profanityFilter, transcriptionLanguage, moreOptionsSize);
            if (response.getResponse() != null) {
                fireTranscriptionEvent(response.getResponse(), response.getConfidence(), transcriptionLanguage, response
                        .getMoreOptions());
            } else {
                fireTranscriptionFailed();
            }
        } catch (IOException e) {
            //e.printStackTrace();
            System.err.println(e.getMessage());
        }
    }

    public interface TranscriptionListener {
        public void transcriptionReceived(String transcription, double confidence, Locale language,
                                          List<String> moreOptions);

        public void transcriptionFailed();
    }
}