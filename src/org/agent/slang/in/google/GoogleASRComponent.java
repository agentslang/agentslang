/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.in.google;

import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.SourceComponent;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.utils.FileUtils;
import org.alvarium.utils.LanguageUtils;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.LineUnavailableException;
import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 2/14/13
 */
@ConfigureParams(optionalConfigurationParams = {"cachePath", "language"},
        outputChannels = "voice.data", outputDataTypes = StringData.class)
public class GoogleASRComponent extends SourceComponent implements GoogleASRRequest.TranscriptionListener {
    private static final String voiceChannel = "voice.data";
    private static final String CACHE_PROP = "cachePath";
    private static final String LANG_PROP = "language";
    private static final AudioFormat defaultAudioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 16000, 16, 2, 4, 16000, false); // Frame rate = 16kHz

    private static final String SESSION = "googleASRSession";

    private MicrophoneRecorder microphoneRecorder;
    private GoogleASRRequest googleASRRequest;

    private File cachePath = new File("cache");

    public GoogleASRComponent(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        this.cachePath = FileUtils.createCacheDir(this, listProperties.getProperty(CACHE_PROP));

        Locale language = LanguageUtils.getLanguage(listProperties.getProperty(LANG_PROP));
        start(defaultAudioFormat, null, language, 10, false);
    }

    public void start(AudioFormat audioFormat, String mixerName, Locale language, int moreOptionsSize,
                      boolean profanityFilter) {
        try {
            googleASRRequest = new GoogleASRRequest(language, moreOptionsSize, profanityFilter);
            googleASRRequest.setName("Google ASR Requester");
            googleASRRequest.addTranscriptionLister(this);
            googleASRRequest.start();

            microphoneRecorder = new MicrophoneRecorder(audioFormat, mixerName, cachePath, googleASRRequest);
            microphoneRecorder.setName("Microphone Capture");
            microphoneRecorder.start();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        if (microphoneRecorder != null) {
            microphoneRecorder.stopThread();
            microphoneRecorder = null;
        }

        if (googleASRRequest != null) {
            googleASRRequest.stopThread();
            googleASRRequest = null;
        }
    }

    private String getMessageID() {
        return UUID.randomUUID().toString();
    }

    public void transcriptionReceived(String transcription, double confidence, Locale language,
                                      List<String> moreOptions) {
        int languageLocale = LanguageUtils.getLanguageCodeByLocale(language);
        publishData(voiceChannel, new StringData(getMessageID(), SESSION, transcription, languageLocale));
        log(this, INFORM, "Google Transcription: " + transcription + " @ confidence = " + confidence);
    }

    public void transcriptionFailed() {
        //ignore for now
        log(this, INFORM, "Google Transcription failed !");
    }

    public void close() {
        super.close();
        stop();
    }
}
