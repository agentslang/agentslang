/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.in.google;

import net.sourceforge.javaflacencoder.FLACFileWriter;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Random;

import static org.alvarium.logger.Logger.Level.DEBUG;
import static org.alvarium.logger.Logger.log;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 9/25/13
 */

public class MicrophoneRecorder extends Thread {
    private static final int DEFAULT_INTERNAL_BUFSIZ = 10240;//40960;

    private AudioFormat audioFormat;
    public TargetDataLine line;

    private boolean recording;

    private int filenameNumber = 0;
    private File cachePath;

    private GoogleASRRequest asrRequest;

    public MicrophoneRecorder(AudioFormat audioFormat, String mixerName, File cachePath,
                              GoogleASRRequest asrRequest) throws LineUnavailableException {
        super();
        this.audioFormat = audioFormat;
        this.cachePath = cachePath;
        this.asrRequest = asrRequest;

        Mixer mixer = null;
        if (mixerName != null) {
            Mixer.Info mixerInfo = VoiceUtility.getMixerInfo(mixerName);
            mixer = AudioSystem.getMixer(mixerInfo);
        }

        DataLine.Info targetInfo = new DataLine.Info(TargetDataLine.class, audioFormat, DEFAULT_INTERNAL_BUFSIZ);
        if (mixer != null) {
            line = (TargetDataLine) mixer.getLine(targetInfo);
        } else {
            line = (TargetDataLine) AudioSystem.getLine(targetInfo);
        }

        line.open(audioFormat, DEFAULT_INTERNAL_BUFSIZ);
    }

    public void start() {
        recording = true;
        line.start();

        super.start();
    }

    public void stopThread() {
        recording = false;
    }

    int sampleNo = 0;
    private static final int TRAINING_THRESHOLD = 30;
    private boolean oldStatus = false;


    private static final int maxFrames = 300;
    private static final int maxBytesOutput = DEFAULT_INTERNAL_BUFSIZ * maxFrames;

    private int outputIndex = 0;
    private byte[] outputStream = new byte[maxBytesOutput];

    private byte[] silenceFrame = new byte[DEFAULT_INTERNAL_BUFSIZ];
    private byte[] previousFrame = new byte[DEFAULT_INTERNAL_BUFSIZ];

    public void run() {
        byte[] abBuffer = new byte[DEFAULT_INTERNAL_BUFSIZ];

        Random rnd = new Random();
        int randomSilenceSample = rnd.nextInt(TRAINING_THRESHOLD);

        while (recording) {
            int nBytesRead = line.read(abBuffer, 0, DEFAULT_INTERNAL_BUFSIZ);

            if (sampleNo < TRAINING_THRESHOLD) {
                train(abBuffer, nBytesRead, audioFormat);
                if (sampleNo == randomSilenceSample) {
                    System.arraycopy(abBuffer, 0, silenceFrame, 0, nBytesRead);
                }
                System.arraycopy(abBuffer, 0, previousFrame, 0, nBytesRead);
            } else {
                boolean newStatus = isVoiced(abBuffer, nBytesRead, audioFormat);
                if (newStatus) {
                    if (outputIndex + nBytesRead > maxBytesOutput) {
                        // end stream - trigger save
                        endBuffer();
                        startNewBuffer();
                    }
                    addSampleToBuffer(abBuffer, nBytesRead);
                }

                if (oldStatus != newStatus) {
                    if (newStatus) {
                        startNewBuffer();
                    } else {
                        endBuffer();
                    }
                }
                oldStatus = newStatus;
                System.arraycopy(abBuffer, 0, previousFrame, 0, nBytesRead);
            }

            sampleNo++;
        }

        line.stop();
        line.close();
        line = null;
    }

    private void addSampleToBuffer(byte[] sample, int sampleSize) {
        System.arraycopy(sample, 0, outputStream, outputIndex, sampleSize);
        outputIndex += sampleSize;
    }

    private void startNewBuffer() {
        log(MicrophoneRecorder.class, DEBUG, "Starting new voiced segment ...");
        outputIndex = 0;
        for (int i = 0; i < UNVOICED_THRESHOLD; i++) {
            addSampleToBuffer(silenceFrame, silenceFrame.length);
        }
        addSampleToBuffer(previousFrame, previousFrame.length);
    }

    private void endBuffer() {
        log(MicrophoneRecorder.class, DEBUG, "Ending voiced segment ...");
        //end stream - trigger save
        asrRequest.pushTranscription(saveToFile());
        outputIndex = 0;
    }

    private File getFilePath() {
        File myFile = new File(cachePath, "voice" + filenameNumber + ".flac");
        while (myFile.exists()) {
            filenameNumber++;
            myFile = new File(cachePath, "voice" + filenameNumber + ".flac");
        }
        return myFile;
    }

    private String saveToFile() {
        int audioLength = outputIndex / audioFormat.getFrameSize();
        AudioInputStream audioInputStream = new AudioInputStream(new ByteArrayInputStream(outputStream), audioFormat, audioLength);
        File myFile = getFilePath();
        try {
            audioInputStream.reset();
        } catch (Exception e) {
            return null;
        }
        try {
            AudioSystem.write(audioInputStream, FLACFileWriter.FLAC, myFile);
        } catch (Exception ex) {
            return null;
        }
        filenameNumber++;
        return myFile.getAbsolutePath();
    }

    //        private float Th_Eavg = 0;
//        private float Th_Emin = Float.MAX_VALUE;
    private float E_MAX = 0;

    private void train(byte[] raw, int bufSize, AudioFormat audioFormat) {
        float rms = VoiceUtility.RMSValue(raw, bufSize, audioFormat);

        E_MAX = Math.max(rms, E_MAX);
        log(MicrophoneRecorder.class, DEBUG, "Training ! Emax = " + E_MAX);
//            Th_Emin = Math.min(rms, Th_Emin);
//            Th_Eavg = (Th_Eavg * sampleNo + rms) / (sampleNo + 1);
//
//            log(MicrophoneRecorder.class, DEBUG,"Training ! Emin = " + Th_Emin + "  Eavg = " + Th_Eavg + "  Emax = " + E_MAX);
    }

    private int unvoicedCount = 0;
    private static final int UNVOICED_THRESHOLD = 4;

    private boolean isVoiced(byte[] raw, int bufSize, AudioFormat audioFormat) {
        if (VoiceUtility.RMSValue(raw, bufSize, audioFormat) > E_MAX) {
            // voiced
            unvoicedCount = 0;
            return true;
        } else if (unvoicedCount > UNVOICED_THRESHOLD) {
            return false;
        } else {
            unvoicedCount++;
            return true;
        }
    }
}