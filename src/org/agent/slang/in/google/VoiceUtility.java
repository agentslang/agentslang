/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.in.google;


import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * ************************************************************
 * Class that submits FLAC audio and retrieves recognized text
 * This is part of the  Java Speech API project:
 * https://github.com/The-Shadow/java-speech-api/
 *
 * @author Luke Kuza, Duncan Jauncey, Aaron Gokaslan
 * @author Ovidiu Serban, oserban@roboslang.org
 *         -> AgentSlang integration as Utility Class
 *         ************************************************************
 */


public class VoiceUtility {
    private static final String GOOGLE_RECOGNIZER_URL = "https://www.google.com/speech-api/v1/recognize?xjerr=1&client=chromium";

    // --user-agent='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7' \ --header='Content-Type: audio/x-flac; rate=16000;'

    public static Response transcribeFromFLAC(File flacFile, boolean profanityFilter, Locale language, int maxResults) throws IOException {
        String response = rawRequest(flacFile, profanityFilter, language, maxResults);
        Response googleResponse = new Response();
        parseResponse(response, googleResponse);
        return googleResponse;
    }

    private static String rawRequest(File inputFile, boolean profanityFilter, Locale language, int maxResults) throws IOException {

        StringBuilder sb = new StringBuilder(GOOGLE_RECOGNIZER_URL);
        if (language != null && language.toString() != null) {
            sb.append("&lang=").append(language.toLanguageTag());
        }
        if (!profanityFilter) {
            sb.append("&pfilter=0");
        }
        sb.append("&maxresults=").append(maxResults);

        URLConnection urlConn = new URL(sb.toString()).openConnection();
        urlConn.setDoOutput(true);
        urlConn.setUseCaches(false);
        urlConn.setRequestProperty("Content-Type", "audio/x-flac; rate=16000");

        OutputStream outputStream = urlConn.getOutputStream();
        FileInputStream fileInputStream = new FileInputStream(inputFile);

        byte[] buffer = new byte[256];
        while ((fileInputStream.read(buffer, 0, 256)) != -1) {
            outputStream.write(buffer, 0, 256);
        }
        fileInputStream.close();
        outputStream.close();

        BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
        sb = new StringBuilder();
        String response;
        while ((response = br.readLine()) != null) {
            sb.append(response).append(" ");
        }
        br.close();

        return sb.toString();
    }

    private static void parseResponse(String rawResponse, Response response) {
        if (!rawResponse.contains("utterance"))
            return;

        String array = substringBetween(rawResponse, "[", "]");
        String[] parts = array.split("}");

        boolean first = true;
        for (String s : parts) {
            if (first) {
                first = false;
                String utterancePart = s.split(",")[0];
                String confidencePart = s.split(",")[1];

                String utterance = utterancePart.split(":")[1];
                String confidence = confidencePart.split(":")[1];

                utterance = stripQuotes(utterance);
                confidence = stripQuotes(confidence);

                if (utterance.equals("null")) {
                    utterance = null;
                }
                if (confidence.equals("null")) {
                    confidence = null;
                }

                response.setResponse(utterance);
                response.setConfidence(confidence);
            } else {
                String utterance = s.split(":")[1];
                utterance = stripQuotes(utterance);
                if (utterance.equals("null")) {
                    utterance = null;
                }
                response.addMoreOptions(utterance);
            }
        }
    }

    private static String substringBetween(String s, String part1, String part2) {
        String sub = null;

        int i = s.indexOf(part1);
        int j = s.indexOf(part2, i + part1.length());

        if (i != -1 && j != -1) {
            int nStart = i + part1.length();
            sub = s.substring(nStart, j);
        }

        return sub;
    }

    private static String stripQuotes(String s) {
        int start = 0;
        if (s.startsWith("\"")) {
            start = 1;
        }
        int end = s.length();
        if (s.endsWith("\"")) {
            end = s.length() - 1;
        }
        return s.substring(start, end);
    }

    public static float RMSValue(byte[] raw, int bufSize, AudioFormat audioFormat) {
        return (float) Math.sqrt(energyValue(raw, bufSize, audioFormat));
    }

    public static float energyValue(byte[] raw, int bufSize, AudioFormat audioFormat) {
        List<float[]> output = new LinkedList<float[]>();
        int floatBufSize = bufSize / audioFormat.getFrameSize();

        FloatSampleTools.byte2float(raw, 0, output, 0, floatBufSize, audioFormat);
        float[] rawFloat = mixDownChannels(output, floatBufSize);

        return energyValue(rawFloat, floatBufSize);
    }

    public static float energyValue(float[] raw, int bufSize) {
        float sum = 0;
        for (int i = 0; i < bufSize; i++) {
            sum += raw[i] * raw[i];
        }
        return sum / raw.length;
    }

    public static float[] mixDownChannels(List<float[]> output, int bufSize) {
        float[] firstChannel = output.get(0);
        int channelCount = output.size();
        for (int ch = 1; ch < channelCount; ch++) {
            float[] thisChannel = output.get(ch);
            for (int i = 0; i < bufSize; i++) {
                firstChannel[i] += thisChannel[i];
            }
        }
        return firstChannel;
    }

    public static Mixer.Info getMixerInfo(String strMixerName) {
        Mixer.Info[] aInfos = AudioSystem.getMixerInfo();
        for (Mixer.Info aInfo : aInfos) {
            if (aInfo.getName().equals(strMixerName)) {
                return aInfo;
            }
        }
        return null;
    }

    public static class Response {
        private String response;
        private Double confidence;
        private List<String> moreOptions = new LinkedList<String>();

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        public Double getConfidence() {
            return confidence;
        }

        public void setConfidence(String confidence) {
            if (confidence == null) {
                this.confidence = 0.0;
            } else {
                try {
                    this.confidence = Double.parseDouble(confidence);
                } catch (NumberFormatException e) {
                    this.confidence = 0.0;
                }
            }
        }

        public List<String> getMoreOptions() {
            return moreOptions;
        }

        public void addMoreOptions(String anotherOption) {
            moreOptions.add(anotherOption);
        }
    }
}