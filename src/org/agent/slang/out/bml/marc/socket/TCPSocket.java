/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.out.bml.marc.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 10/19/13
 */
public class TCPSocket extends MarcSocket {
    private Socket inSocket;
    private ServerSocket outSocket;
    private PrintWriter inWriter;
    private boolean running = true;

    public TCPSocket(String hostname, int inPort, int outPort, DataListener listener) throws IOException {
        super(hostname, inPort, outPort);

        inSocket = new Socket(this.hostname, inPort);
        outSocket = new ServerSocket(outPort);

        inWriter = new PrintWriter(inSocket.getOutputStream(), true);

        addDataListener(listener);
        start();
    }

    public void sendMessage(String message) {
        if (inWriter != null && inSocket != null && inSocket.isConnected()) {
            inWriter.println(message);
            log(TCPSocket.class, INFORM, "Socket message sent: " + message);
        } else {
            log(TCPSocket.class, INFORM, "Failed to send socket message: " + message);
        }
    }

    public boolean isConnected() {
        return inSocket != null && inSocket.isConnected();
    }

    public void run() {
        while (running) {
            try {
                Socket client = outSocket.accept();
                log(TCPSocket.class, INFORM, "Accepted connection from: " + client.getRemoteSocketAddress().toString());
                BufferedReader outReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                String line;
                try {
                    while ((line = outReader.readLine()) != null) {
                        fireDataUpdate(line);
                    }
                    outReader.close();
                    client.close();
                } catch (IOException e) {
                    //-- ignore for now
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void close() {
        super.close();
        running = false;

        if (inWriter != null) {
            inWriter.close();
        }

        if (inSocket != null && inSocket.isConnected()) {
            try {
                inSocket.close();
            } catch (IOException e) {
                //-- ignore
            }
        }

        if (outSocket != null) {
            try {
                outSocket.close();
            } catch (IOException e) {
                //-- ignore
            }
        }
    }
}
