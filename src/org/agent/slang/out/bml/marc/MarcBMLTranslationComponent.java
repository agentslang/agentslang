/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.out.bml.marc;

import org.agent.slang.data.audio.AudioData;
import org.agent.slang.data.audio.PlayerEvent;
import org.agent.slang.out.bml.marc.socket.MarcSocket;
import org.agent.slang.out.bml.marc.socket.TCPSocket;
import org.agent.slang.out.bml.marc.socket.UDPSocket;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.utils.FileUtils;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import static org.agent.slang.util.PropertyUtils.getFilename;
import static org.agent.slang.util.PropertyUtils.getIntegerProperty;
import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 10/19/13
 */

@ConfigureParams(optionalConfigurationParams = {"MARCSocketType", "MARCHostname", "MARCInPort", "MARCOutPort"},
        outputChannels = "audioPlayer.data", outputDataTypes = PlayerEvent.class,
        inputDataTypes = AudioData.class)
public class MarcBMLTranslationComponent extends MixedComponent implements MarcSocket.DataListener {
    private static final String PROP_SOCKET_TYPE = "MARCSocketType";
    private static final String PROP_SOCKET_HOST = "MARCHostname";
    private static final String PROP_SOCKET_IN_PORT = "MARCInPort";
    private static final String PROP_SOCKET_OUT_PORT = "MARCOutPort";
    private static final String PROP_CACHE = "audioCache";
    private static final String DEFAULT_CACHE = "MARCAudioCache";

    private static final String SESSION = "marcSession";
    //topics
    private static final String audioPlayerData = "audioPlayer.data";
    private final static long EXECUTION_TIMEOUT = 5 * 60 * 1000;
    private final Queue<String> bmlExecutionQueue = new LinkedList<>();
    //MARC socket
    private MarcSocket socket;
    private File audioCache;
    private long bmlID = 0;
    private Long lastExecutionTimestamp = null;

    public MarcBMLTranslationComponent(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        String hostname = listProperties.getProperty(PROP_SOCKET_HOST, "localhost");
        int intPort = getIntegerProperty(listProperties.getProperty(PROP_SOCKET_IN_PORT), 4111);
        int outPort = getIntegerProperty(listProperties.getProperty(PROP_SOCKET_OUT_PORT), 4112);

        log(this, INFORM, "Using MARC Connections Properties hostname='%s' inPort=%d outPort=%d", hostname, intPort, outPort);

        if ("tcp".equals(listProperties.getProperty(PROP_SOCKET_TYPE))) {
            try {
                socket = new TCPSocket(hostname, intPort, outPort, this);
                log(this, INFORM, "Using MARC with a TCP Connection ...");
            } catch (IOException e) {
                log(this, CRITICAL, "Invalid properties for socket", e);
            }
        } else {
            try {
                socket = new UDPSocket(hostname, intPort, outPort, this);
                log(this, INFORM, "Using MARC with a UDP Connection ...");
            } catch (IOException e) {
                log(this, CRITICAL, "Invalid properties for socket", e);
            }
        }

        audioCache = FileUtils.createCacheDir(this, getFilename(listProperties.getProperty(PROP_CACHE, DEFAULT_CACHE)));
        log(this, INFORM, "MARC Audio Cache Path = " + audioCache.getAbsolutePath());
    }


    public void dataReceived(String message) {
        log(this, INFORM, "From MARC: " + message);
        if (message.toLowerCase().contains("bml:start")) {
            publishData(audioPlayerData, new PlayerEvent("0", SESSION, PlayerEvent.EVENT_START));
            log(this, INFORM, "Sending player start message !");
        } else if (message.toLowerCase().contains("bml:end")) {
            publishData(audioPlayerData, new PlayerEvent("0", SESSION, PlayerEvent.EVENT_STOP));
            log(this, INFORM, "Sending player stop message !");
            stopAndScheduleNext();
        }
    }

    protected void handleData(GenericData data) {
        String bmlID = generateBMLID();
        String audioPath = saveToFile(bmlID, (AudioData) data);
        if (audioPath != null) {
            String message = generateBML(bmlID, audioPath);
            scheduleForExecutionMessage(message);
        }
    }

    private void scheduleForExecutionMessage(String message) {
        synchronized (bmlExecutionQueue) {
            bmlExecutionQueue.offer(message);
            scheduleNextBML();
        }
    }

    private void scheduleNextBML() {
        synchronized (bmlExecutionQueue) {
            if (lastExecutionTimestamp == null || System
                    .currentTimeMillis() - lastExecutionTimestamp > EXECUTION_TIMEOUT) {
                String message = bmlExecutionQueue.poll();
                if (message != null) {
                    if (socket != null) {
                        log(this, INFORM, "MARC Sending message: " + message);
                        socket.sendMessage(message);
                        lastExecutionTimestamp = System.currentTimeMillis();
                    } else {
                        log(this, INFORM, "Marc Sending message failed: " + message);
                    }
                } else {
                    lastExecutionTimestamp = null;
                }
            }
        }
    }

    private void stopAndScheduleNext() {
        synchronized (bmlExecutionQueue) {
            lastExecutionTimestamp = null;
            scheduleNextBML();
        }
    }

    private String generateBMLID() {
        return "bml-" + (bmlID++);
    }

    private String saveToFile(String bmlID, AudioData data) {
        File audioFileData = new File(audioCache, "audio-" + bmlID + ".wav");
        try {
            AudioSystem.write(data.getAudioStream(), AudioFileFormat.Type.WAVE, audioFileData);
            return audioFileData.getAbsolutePath();
        } catch (IOException e) {
            log(this, CRITICAL, "Invalid save path = " + audioFileData.getAbsolutePath());
            return null;
        }
    }

    private String generateBML(String bmlID, String audioPath) {
        return "<bml><speech id=\"" + bmlID + "\" marc:file=\"" + audioPath + "\" marc:articulate=\"1.00\" /></bml>";
    }

    public void close() {
        super.close();

        if (socket != null) {
            socket.close();
        }
    }
}
