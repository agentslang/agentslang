/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.out.ispeech;

import com.iSpeech.ApiException;
import com.iSpeech.InvalidApiKeyException;
import com.iSpeech.TTSResult;
import com.iSpeech.iSpeechSynthesis;
import org.agent.slang.data.annotation.GenericTextAnnotation;
import org.agent.slang.data.audio.AudioData;
import org.agent.slang.data.audio.InvalidAudioDataException;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.SessionDependentData;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.log;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 2/8/13
 *          <p>
 *          Not tested since the transition to the new API
 */

@ConfigureParams(mandatoryConfigurationParams = {"voice", "apiKey"},
        outputChannels = "voice.data", outputDataTypes = AudioData.class,
        inputDataTypes = {StringData.class, GenericTextAnnotation.class})
public class ISpeechTTSComponent extends MixedComponent {
    private static final String voiceChannel = "voice.data";
    private static final String PROP_VOICE = "voice";
    private static final String PROP_API_KEY = "apiKey";
    private iSpeechSynthesis iSpeechTTS;

    public ISpeechTTSComponent(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        try {
            iSpeechTTS = iSpeechSynthesis.getInstance(listProperties.getProperty(PROP_API_KEY), false);
            iSpeechTTS.setOptionalCommand("format", "wav");

            if (listProperties.hasProperty(PROP_VOICE)) {
                iSpeechTTS.setOptionalCommand("voice", listProperties.getProperty(PROP_VOICE));
            } else {
                iSpeechTTS.setOptionalCommand("voice", "usenglishfemale");
            }
        } catch (InvalidApiKeyException e) {
            log(this, CRITICAL, "Invalid iSpeech API Key ", e);
        }
    }

    protected void handleData(GenericData data) {
        if (data != null) {
            AudioData result = null;
            String sessionId = ((SessionDependentData) data).getSessionId();
            if (data instanceof StringData) {
                try {
                    AudioInputStream ais = buildAudioStream(((StringData) data).getData());
                    if (ais != null) {
                        result = new AudioData(data.getId(), sessionId, ((StringData) data).getData());
                        result.buildAudioData(ais);
                    }
                } catch (InvalidAudioDataException e) {
                    log(this, CRITICAL, "Invalid audio data provided ", e);
                }
            } else if (data instanceof GenericTextAnnotation) {
                try {
                    AudioInputStream ais = buildAudioStream(((GenericTextAnnotation) data).getTranscription());
                    if (ais != null) {
                        result = new AudioData(data.getId(), sessionId,
                                               ((GenericTextAnnotation) data).getTranscription());
                        result.buildAudioData(ais);
                    }
                } catch (InvalidAudioDataException e) {
                    log(this, CRITICAL, "Invalid audio data provided ", e);
                }
            }
            if (result != null) {
                publishData(voiceChannel, result);
            }
        }
    }

    private AudioInputStream buildAudioStream(String transcription) {
        try {
            TTSResult result = iSpeechTTS.speak(transcription);
            AudioFormat af = new AudioFormat(16000.0f, 16, 1, true, false);
            DataInputStream in = result.getDataInputStream();
            AudioInputStream as = new AudioInputStream(in, af, result.getAudioFileLength());
            return new AudioInputStream(as, af, result.getAudioFileLength());
        } catch (IOException | ApiException e) {
            log(this, CRITICAL, "Invalid audio data provided ", e);
        }
        return null;
    }
}
