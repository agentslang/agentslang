/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.out.marytts;

import marytts.LocalMaryInterface;
import marytts.MaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;
import org.agent.slang.data.annotation.GenericTextAnnotation;
import org.agent.slang.data.audio.AudioData;
import org.agent.slang.data.audio.InvalidAudioDataException;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.SessionDependentData;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;

import java.util.Locale;

import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 12/8/12
 */
@ConfigureParams(optionalConfigurationParams = "locale",
        outputChannels = "voice.data", outputDataTypes = AudioData.class,
        inputDataTypes = {StringData.class, GenericTextAnnotation.class})
public class MaryComponent extends MixedComponent {
    private static final String voiceChannel = "voice.data";
    private static final String PROP_LOCALE = "locale";
    private MaryInterface maryInterface;

    public MaryComponent(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        try {
            maryInterface = new LocalMaryInterface();
            log(this, INFORM, "I currently have " + maryInterface.getAvailableVoices() + " voices in "
                    + maryInterface.getAvailableLocales() + " languages available.");
            log(this, INFORM, "Out of these, " + maryInterface.getAvailableVoices(Locale.US) + " are for US English.");

            //test the generation
            try {
                if (AudioConsumer.consume(maryInterface.generateAudio("This is just a test"), true)) {
                    log(this, INFORM, "MaryTTS is up and ready to go ...");
                } else {
                    log(this, CRITICAL, "Something went wrong with MaryTTS. Check the config and try again ...");
                }
            } catch (SynthesisException e) {
                log(this, CRITICAL, "Invalid synthesis ", e);
            }

            if (listProperties.hasProperty(PROP_LOCALE)) {
                String[] parts = listProperties.getProperty(PROP_LOCALE).split("_");
                String lang = parts[0];
                String country = parts[0];
                if (parts.length > 1) {
                    country = parts[1];
                }

                Locale locale = new Locale(lang, country);
                maryInterface.setLocale(locale);
            } else {
                maryInterface.setLocale(Locale.US);
            }
        } catch (MaryConfigurationException e) {
            log(this, CRITICAL, "Invalid Mary configuration", e);
        }
    }

    protected void handleData(GenericData data) {
        if (data != null) {
            String sessionId = ((SessionDependentData) data).getSessionId();
            AudioData result = null;
            if (data instanceof StringData) {
                result = new AudioData(data.getId(), sessionId, ((StringData) data).getData());
                try {
                    result.buildAudioData(maryInterface.generateAudio(result.getTextTranscription()));
                } catch (InvalidAudioDataException e) {
                    log(this, CRITICAL, "Invalid audio data provided ", e);
                } catch (SynthesisException e) {
                    log(this, CRITICAL, "Invalid synthesis ", e);
                }
            } else if (data instanceof GenericTextAnnotation) {
                result = new AudioData(data.getId(), sessionId, ((GenericTextAnnotation) data).getTranscription());
                try {
                    result.buildAudioData(maryInterface.generateAudio(result.getTextTranscription()));
                } catch (InvalidAudioDataException e) {
                    log(this, CRITICAL, "Invalid audio data provided ", e);
                } catch (SynthesisException e) {
                    log(this, CRITICAL, "Invalid synthesis ", e);
                }
            }
            if (result != null) {
                publishData(voiceChannel, result);
            }
        }
    }
}
