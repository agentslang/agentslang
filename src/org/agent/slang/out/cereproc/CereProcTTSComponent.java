/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.out.cereproc;

import com.cereproc.cerevoice_eng.*;
import org.agent.slang.data.annotation.GenericTextAnnotation;
import org.agent.slang.data.audio.AudioData;
import org.agent.slang.data.audio.InvalidAudioDataException;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.SessionDependentData;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.utils.FileUtils;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.Charset;

import static org.agent.slang.util.PropertyUtils.getFilename;
import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 9/27/13
 */

@ConfigureParams(mandatoryConfigurationParams = {"voice", "licenseFile"},
        outputChannels = "voice.data", outputDataTypes = AudioData.class,
        inputDataTypes = {StringData.class, GenericTextAnnotation.class})
public class CereProcTTSComponent extends MixedComponent {
    private static final String voiceChannel = "voice.data";
    private static final String PROP_VOICE = "voice";
    private static final String PROP_LICENSE_FILE = "licenseFile";

    static {
        System.loadLibrary("cerevoice_eng");
    }

    private SWIGTYPE_p_CPRCEN_engine cereProcEngine;
    private int chanelHandle;
    private float sampleRate;
    //    private CereProcCallback callback;
    private AudioFormat defaultFormat = null;

    public CereProcTTSComponent(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        File licencePath = getFilename(listProperties.getProperty(PROP_LICENSE_FILE));
        FileUtils.checkReadableFile(licencePath, true);

        File voicePath = getFilename(listProperties.getProperty(PROP_VOICE));
        FileUtils.checkReadableFile(voicePath, true);

        cereProcEngine = cerevoice_eng.CPRCEN_engine_new();
        int res = cerevoice_eng.CPRCEN_engine_load_voice(cereProcEngine, licencePath.getAbsolutePath(), "", voicePath
                .getAbsolutePath(), CPRC_VOICE_LOAD_TYPE.CPRC_VOICE_LOAD);
        if (res == 0) {
            log(this, CRITICAL, "Invalid CereProc License or voice model provided.");
        }

        chanelHandle = cerevoice_eng.CPRCEN_engine_open_default_channel(cereProcEngine);
        if (chanelHandle == 0) {
            log(this, CRITICAL, "Unable to open the default CereProc model.");
        }

        sampleRate = Float
                .parseFloat(cerevoice_eng.CPRCEN_channel_get_voice_info(cereProcEngine, chanelHandle, "SAMPLE_RATE"));
    }

    protected void handleData(GenericData data) {
        if (data != null) {
            String sessionId = ((SessionDependentData) data).getSessionId();
            if (data instanceof StringData) {
                buildAudioStream(data.getId(), sessionId, ((StringData) data).getData());
            } else if (data instanceof GenericTextAnnotation) {
                buildAudioStream(data.getId(), sessionId, ((GenericTextAnnotation) data).getTranscription());
            }
        }
    }

    private synchronized void buildAudioStream(String id, String sessionId, String transcription) {
        transcription = transcription + "\n";
        byte[] transcriptionBytes = transcription.getBytes(Charset.forName("UTF-8"));
        processBuffer(id, sessionId, transcription, cerevoice_eng
                .CPRCEN_engine_channel_speak(cereProcEngine, chanelHandle, transcription, transcriptionBytes.length, 1));
        cerevoice_eng.CPRCEN_engine_clear_callback(cereProcEngine, chanelHandle);
    }

    private synchronized void sendTranscription(String id, String sessionId, String transcription, byte[] buffer,
                                                String phonemes) {
        try {
            AudioFormat format = getAudioFormat();
            AudioInputStream ais = new AudioInputStream(new ByteArrayInputStream(buffer), format,
                                                        buffer.length / format.getFrameSize());
            AudioData result = new AudioData(id, sessionId, transcription);
            result.buildAudioData(ais);
            publishData(voiceChannel, result);

            log(this, INFORM, "CereProc Phonemes: " + phonemes);
        } catch (InvalidAudioDataException e) {
            log(this, CRITICAL, "CereProc exception while sending transcription.", e);
        }
    }

    private void processBuffer(String id, String sessionId, String transcription, SWIGTYPE_p_CPRC_abuf cbuffer) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sbM = new StringBuilder();
        for (int i = 0; i < cerevoice_eng.CPRC_abuf_trans_sz(cbuffer); i++) {
            SWIGTYPE_p_CPRC_abuf_trans trans = cerevoice_eng.CPRC_abuf_get_trans(cbuffer, i);
            CPRC_ABUF_TRANS_TYPE transtype = cerevoice_eng.CPRC_abuf_trans_type(trans);
            float start = cerevoice_eng.CPRC_abuf_trans_start(trans);
            float end = cerevoice_eng.CPRC_abuf_trans_end(trans);
            String name = cerevoice_eng.CPRC_abuf_trans_name(trans);

            if (transtype == CPRC_ABUF_TRANS_TYPE.CPRC_ABUF_TRANS_PHONE) {
                sb.append(String.format("%.3f %.3f %s ", start, end, name));
            } else if (transtype == CPRC_ABUF_TRANS_TYPE.CPRC_ABUF_TRANS_MARK) {
                sbM.append(String.format("%.3f %.3f %s ", start, end, name));
            }
        }

        log(this, INFORM, "Phrase marks = " + sbM.toString());

        int bufferSize = cerevoice_eng.CPRC_abuf_wav_sz(cbuffer);
        byte[] b = new byte[bufferSize * 2];
        for (int i = 0; i < bufferSize; i++) {
            short sample = cerevoice_eng.CPRC_abuf_wav(cbuffer, i);
            // The sample is written in Big Endian to the buffer
            b[i * 2] = (byte) ((sample & 0xff00) >> 8);
            b[i * 2 + 1] = (byte) (sample & 0x00ff);
        }

        sendTranscription(id, sessionId, transcription, b, sb.toString());
    }

    private AudioFormat getAudioFormat() {
        if (defaultFormat == null || defaultFormat.getSampleRate() != sampleRate) {
            int sampleSizeInBits = 16;
            int channels = 1;
            boolean signed = true;
            boolean bigEndian = true;
            defaultFormat = new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
        }
        return defaultFormat;
    }

    public void close() {
        super.close();

        cerevoice_eng.CPRCEN_engine_channel_close(cereProcEngine, chanelHandle);
        cerevoice_eng.CPRCEN_engine_delete(cereProcEngine);
    }
}
