package org.agent.slang.util;

import net.sourceforge.lame.lowlevel.LameEncoder;

import javax.sound.sampled.AudioInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static net.sourceforge.lame.lowlevel.LameEncoder.BITRATE_AUTO;
import static net.sourceforge.lame.mp3.Lame.QUALITY_HIGHEST;
import static net.sourceforge.lame.mp3.MPEGMode.STEREO;


public class Mp3Util {
    public static void saveMp3File(File filename, AudioInputStream audioInputStream) throws IOException {
        LameEncoder encoder = new LameEncoder(audioInputStream.getFormat(),
                                              BITRATE_AUTO, STEREO, QUALITY_HIGHEST, false);

        byte[] pcm = new byte[audioInputStream.available()];
        audioInputStream.read(pcm);

        FileOutputStream mp3 = new FileOutputStream(filename);
        byte[] buffer = new byte[encoder.getPCMBufferSize()];

        int bytesToTransfer = Math.min(buffer.length, pcm.length);
        int bytesWritten;
        int currentPcmPosition = 0;
        while (0 < (bytesWritten = encoder.encodeBuffer(pcm, currentPcmPosition, bytesToTransfer, buffer))) {
            currentPcmPosition += bytesToTransfer;
            bytesToTransfer = Math.min(buffer.length, pcm.length - currentPcmPosition);

            mp3.write(buffer, 0, bytesWritten);
        }

        encoder.close();
        mp3.close();
    }
}
