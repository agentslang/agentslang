package org.agent.slang.util;

import java.io.File;
import java.io.IOException;

import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.log;

public class PropertyUtils {
    public static File getFilename(String path) {
        if (path == null) {
            return null;
        } else {
            try {
                return new File(path).getCanonicalFile();
            } catch (IOException e) {
                log(PropertyUtils.class, CRITICAL, "getFilename exception", e);
                return null;
            }
        }
    }

    public static File getFilename(String parent, String child) {
        if (child == null) {
            return null;
        } else {
            try {
                return new File(parent, child).getCanonicalFile();
            } catch (IOException e) {
                log(PropertyUtils.class, CRITICAL, "getFilename exception", e);
                return null;
            }
        }
    }

    public static boolean getBooleanProperty(String value) {
        return "true".equals(value.toLowerCase().trim());
    }

    public static Integer getIntegerProperty(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static int getIntegerProperty(String value, int defaultValue) {
        if (value == null) {
            return defaultValue;
        }

        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }
}
