package org.agent.slang.util;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.Reader;
import java.io.Writer;

public class XmlUtils {
    // serialize and deserialize with JAXB

    public static void serializeToXml(Object xmlObject, Writer writer) {
        try {
            Marshaller marshaller = JAXBContext.newInstance(xmlObject.getClass()).createMarshaller();
            marshaller.marshal(xmlObject, writer);
        } catch (JAXBException e) {
            throw new IllegalArgumentException("Invalid class provided. Class may not be annotated properly", e);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T deserializeFromXml(Class<T> clazz, Reader reader) {
        try {
            Unmarshaller unmarshaller = JAXBContext.newInstance(clazz).createUnmarshaller();
            return (T) unmarshaller.unmarshal(reader);
        } catch (JAXBException e) {
            throw new IllegalArgumentException("Invalid class provided. Class may not be annotated properly", e);
        }
    }
}
