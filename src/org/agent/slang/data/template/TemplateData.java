/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.data.template;


import org.alvarium.component.data.IdentifiableData;
import org.alvarium.component.data.LanguageDependentData;
import org.alvarium.component.data.SessionDependentData;
import org.alvarium.component.data.TypeIdentification;
import org.alvarium.utils.LanguageUtils;

import java.util.*;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 12/31/12
 */

@TypeIdentification(typeID = 5)
public class TemplateData implements IdentifiableData, SessionDependentData, LanguageDependentData {
    private String id;
    private String sessionId;
    private int language;
    private List<String> templateIds = new LinkedList<String>();
    private Map<String, String> extractedVars = new HashMap<String, String>();

    public TemplateData() {
    }

    public TemplateData(String id, String sessionId, int language) {
        this.id = id;
        this.sessionId = sessionId;
        this.language = language;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getSessionId() {
        return sessionId;
    }

    @Override
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public int getLanguage() {
        return language;
    }

    public void setLanguage(int language) {
        this.language = language;
    }

    public Locale getLocale() {
        return LanguageUtils.getLocaleByCode(language);
    }

    public void addTemplateId(String templateID) {
        templateIds.add(templateID);
    }

    public void addTemplateIds(List<String> templateIds) {
        this.templateIds.addAll(templateIds);
    }

    public void updateVariables(Map<String, String> extractedVars) {
        this.extractedVars.putAll(extractedVars);
    }

    public Map<String, String> getExtractedVars() {
        return extractedVars;
    }

    public void setExtractedVars(Map<String, String> extractedVars) {
        this.extractedVars = extractedVars;
    }

    public List<String> getTemplateIDs() {
        return templateIds;
    }

    public boolean isEmpty() {
        return templateIds.isEmpty();
    }

    public List<String> getTemplateIds() {
        return templateIds;
    }

    public void setTemplateIds(List<String> templateIds) {
        this.templateIds = templateIds;
    }

    public String toString() {
        return "(" + id + ") " + templateIds + variablesToString();
    }

    private String variablesToString() {
        if (extractedVars.isEmpty()) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder(" @Variables:");
            for (Map.Entry<String, String> item : extractedVars.entrySet()) {
                if (sb.length() > 0) {
                    sb.append(" ");
                }
                sb.append(item.getKey()).append("=").append(item.getValue());
            }
            sb.append(" @");
            return sb.toString();
        }
    }
}
