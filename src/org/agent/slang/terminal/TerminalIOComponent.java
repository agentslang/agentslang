/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.terminal;

import org.agent.slang.audio.AudioPlayer;
import org.agent.slang.data.audio.AudioData;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.utils.FileUtils;
import org.alvarium.utils.LanguageUtils;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.agent.slang.util.PropertyUtils.getBooleanProperty;
import static org.agent.slang.util.PropertyUtils.getFilename;
import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.log;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 11/19/13
 */

@ConfigureParams(optionalConfigurationParams = {"inputFilename", "outputFilename", "audioCache", "inputMultiline"},
        outputChannels = "terminal.data", outputDataTypes = StringData.class,
        inputDataTypes = GenericData.class)
public class TerminalIOComponent extends MixedComponent implements AudioPlayer.AudioPlayerListener {
    private static final String terminalChannel = "terminal.data";
    private static final String PROP_INPUT_FILE = "inputFilename";
    private static final String PROP_INPUT_MULTILINE = "inputMultiline";
    private static final String PROP_OUTPUT_FILE = "outputFilename";
    private static final String PROP_AUDIO_CACHE = "audioCache";

    private static final String SESSION = "terminal";

    private Thread terminalThread;
    private boolean running = true;
    private int languageCode = LanguageUtils.getLanguageCodeByLocale(LanguageUtils.getLanguage("en-US"));

    private File inputFilename;
    private boolean inputMultiline;
    private File outputFilename;
    private PrintWriter out;
    private File audioCache;

    private Map<String, String> filenameMapping = new HashMap<>();
    private AudioPlayer audioPlayer = new AudioPlayer();

    public TerminalIOComponent(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        inputFilename = getFilename(listProperties.getProperty(PROP_INPUT_FILE));

        if (inputFilename != null) {
            FileUtils.checkReadableFile(inputFilename, true);
        }

        outputFilename = getFilename(listProperties.getProperty(PROP_OUTPUT_FILE));
        if (outputFilename != null) {
            FileUtils.checkWritableFile(outputFilename, true);
        }

        inputMultiline = getBooleanProperty(listProperties.getProperty(PROP_INPUT_MULTILINE, "false"));

        audioCache = FileUtils.createCacheDir(this, listProperties.getProperty(PROP_AUDIO_CACHE, ""));

        audioPlayer.addStatusListener(this);

        if (outputFilename != null) {
            try {
                out = new PrintWriter(new FileWriter(outputFilename), true);
            } catch (IOException e) {
                log(this, CRITICAL, "File Not Found: " + outputFilename.getAbsolutePath(), e);
            }
        } else {
            out = new PrintWriter(System.out, true);
        }

        terminalThread = new TerminalThread();
        terminalThread.start();
    }


    private String getMessageID() {
        return UUID.randomUUID().toString();
    }

    private String saveToFile(String fileID, AudioData data) {
        File audioFileData;
        if (filenameMapping.containsKey(fileID)) {
            audioFileData = new File(audioCache, filenameMapping.get(fileID));
        } else {
            audioFileData = new File(audioCache, "audio-" + fileID + ".wav");
        }
        try {
            AudioSystem.write(data.getAudioStream(), AudioFileFormat.Type.WAVE, audioFileData);
            return audioFileData.getAbsolutePath();
        } catch (IOException e) {
            System.err.println("Invalid save path = " + audioFileData.getAbsolutePath());
            return null;
        }
    }

    protected void handleData(GenericData data) {
        if (data instanceof AudioData) {
            //save the data
            String filename = saveToFile(data.getId(), (AudioData) data);
            out.println("Audio Data saved: " + filename);
            audioPlayer.playAudio(filename, ((AudioData) data).getAudioStream());
        } else {
            out.println(data.toString());
        }
    }

    public void audioPlayerStatusUpdated(String audioLabel, AudioPlayer.AudioPlayerEvent event) {
        if (event == AudioPlayer.AudioPlayerEvent.start) {
            out.println("Playing audio data: " + audioLabel);
        } else if (event == AudioPlayer.AudioPlayerEvent.stop) {
            out.println("Stopping audio player ...");
        }
    }

    public void close() {
        super.close();
        running = false;
        audioPlayer.removeStatusListener(this);
        terminalThread.interrupt();
        out.close();
        audioPlayer.killPlayer();
    }

    private class TerminalThread extends Thread {
        TerminalThread() {
            super("Terminal Thread");
        }

        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                //-- wait for the terminal to be active
            }
            BufferedReader reader = null;
            if (inputFilename == null) {
                reader = new BufferedReader(new InputStreamReader(System.in));
            } else {
                try {
                    reader = new BufferedReader(new FileReader(inputFilename));
                } catch (FileNotFoundException e) {
                    System.err.println("File Not Found: " + inputFilename.getAbsolutePath());
                    System.exit(1);
                }
            }
            try {
                String line;
                System.out.println("Starting TerminalIO ... using:");
                System.out.println("\tInput: " + (inputFilename == null ? "terminal" : inputFilename
                        .getAbsolutePath()));
                System.out.println("\tOutput: " + (outputFilename == null ? "terminal" : outputFilename
                        .getAbsolutePath()));
                System.out.println("\tMulti-line input: " + inputMultiline);
                StringBuilder sb = new StringBuilder();
                while (running && ((line = reader.readLine()) != null)) {
                    if (line.startsWith("#")) {
                        if (line.contains("publishFile=")) {
                            String messageID = getMessageID();
                            filenameMapping.put(messageID, line.substring(line.indexOf('=') + 1).trim());
                            publishData(terminalChannel, new StringData(messageID, SESSION,
                                                                        sb.toString(), languageCode));
                            sb = new StringBuilder();
                        }
                        continue;
                    }

                    if (inputMultiline) {
                        sb.append(line);//.append("\n");
                    } else {
                        publishData(terminalChannel, new StringData(getMessageID(), SESSION, line, languageCode));
                    }
                }
                if (inputMultiline && sb.length() > 0) {
                    publishData(terminalChannel, new StringData(getMessageID(), SESSION, sb.toString(), languageCode));
                }
                reader.close();
                System.out.println("TerminalIO stopped !");
            } catch (IOException e) {
                //-- ignore
            }
        }
    }
}
