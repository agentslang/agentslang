/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.terminal;

import org.agent.slang.audio.AudioPlayer;
import org.agent.slang.data.audio.AudioData;
import org.agent.slang.data.audio.PlayerEvent;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.container.monitor.MonitorChangeEvent;
import org.alvarium.container.monitor.MonitorChangeListener;
import org.alvarium.utils.LanguageUtils;

import java.util.Locale;
import java.util.UUID;

import static org.alvarium.container.monitor.SystemMonitorManager.systemMonitorManager;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 12/8/12
 */
@ConfigureParams(outputChannels = {"text.data", "audioPlayer.data"},
        outputDataTypes = {StringData.class, PlayerEvent.class},
        inputDataTypes = {GenericData.class})
public class TextComponent extends MixedComponent implements AudioPlayer.AudioPlayerListener, MonitorChangeListener {
    private static final String textData = "text.data";
    private static final String audioPlayerData = "audioPlayer.data";

    private static final String SESSION = "terminal";

    private TextTerminal textTerminal;
    private AudioPlayer audioPlayer;

    public TextComponent(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        textTerminal = new TextTerminal(this);
        textTerminal.setVisible(true);
        textTerminal.setLocationRelativeTo(null);

        audioPlayer = new AudioPlayer();
        audioPlayer.addStatusListener(this);

        systemMonitorManager.addListener(this);
    }

    protected void handleData(GenericData data) {
        if (data instanceof AudioData) {
            if (((AudioData) data).getAudioStream() != null) {
                audioPlayer.playAudio(((AudioData) data).getTextTranscription(), ((AudioData) data).getAudioStream());
            }
        } else if (data != null) {
            textTerminal.handleMessage(data.toString());
        }
    }

    @Override
    public void containerEventFired(MonitorChangeEvent monitorChangeEvent) {
        if (monitorChangeEvent.getEventType() != MonitorChangeEvent.EventType.act) {
            textTerminal.handleMessage("Container id = " + monitorChangeEvent.getContainerId()
                                               + " fired an event = " + monitorChangeEvent.getEventType());
        }
    }

    @Override
    public void componentEventFired(MonitorChangeEvent monitorChangeEvent) {
        if (monitorChangeEvent.getEventType() != MonitorChangeEvent.EventType.act) {
            textTerminal.handleMessage("Component id = " + monitorChangeEvent.getContainerId() + "::" +
                                               monitorChangeEvent.getComponentId() + " fired an event = " +
                                               monitorChangeEvent.getEventType());
        }
    }

    public void audioPlayerStatusUpdated(String audioLabel, AudioPlayer.AudioPlayerEvent event) {
        if (event == AudioPlayer.AudioPlayerEvent.start) {
            textTerminal.handleMessage("** Playing audio data: " + audioLabel);
            publishData(audioPlayerData, new PlayerEvent(getMessageID(), SESSION, PlayerEvent.EVENT_START));
        } else if (event == AudioPlayer.AudioPlayerEvent.stop) {
            textTerminal.handleMessage("** Stopping audio player ...");
            publishData(audioPlayerData, new PlayerEvent(getMessageID(), SESSION, PlayerEvent.EVENT_STOP));
        }
    }

    private String getMessageID() {
        return UUID.randomUUID().toString();
    }

    void sendMessage(String message) {
        int langCode = LanguageUtils.getLanguageCodeByLocale(Locale.US);
        publishData(textData, new StringData(getMessageID(), SESSION, message, langCode));
    }
}
