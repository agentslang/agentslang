package org.agent.slang.ws.message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class WsMessageFactory {
    private static final ObjectMapper mapper = new ObjectMapper();

    public static String serializeToJSON(WsMessage message) throws JsonProcessingException {
        return mapper.writeValueAsString(message);
    }

    public static <T> T deserializeFromJSON(String message, Class<T> clazz) throws IOException {
        return mapper.readValue(message, clazz);
    }
}
