package org.agent.slang.ws.message;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class WsMessage {
    private WsMessageType type;
    private String message;
    private String voiceUrl;

    @JsonIgnore
    private String sessionId;

    public WsMessage(WsMessageType type, String message, String voiceUrl) {
        this.type = type;
        this.message = message;
        this.voiceUrl = voiceUrl;
    }

    public WsMessage() {
    }

    public WsMessageType getType() {
        return type;
    }

    public void setType(WsMessageType type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVoiceUrl() {
        return voiceUrl;
    }

    public void setVoiceUrl(String voiceUrl) {
        this.voiceUrl = voiceUrl;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        return "WsMessage{" +
                "type=" + type +
                ", message='" + message + '\'' +
                ", voiceUrl='" + voiceUrl + '\'' +
                ", sessionId='" + sessionId + '\'' +
                '}';
    }

}
