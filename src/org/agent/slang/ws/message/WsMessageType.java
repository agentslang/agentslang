package org.agent.slang.ws.message;

public enum WsMessageType {
    message,
    debug,
    system
}
