package org.agent.slang.ws;

import org.agent.slang.ws.message.WsMessage;
import org.alvarium.utils.Pair;

import javax.websocket.Session;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;

class WebFrontendManager {
    static final WebFrontendManager webFrontendManager = new WebFrontendManager();

    private static final List<WsMessageReceiveListener> receiveListeners = new LinkedList<>();

    private static final Map<String, Pair<Session, WsMessagePushListener>> connectedSessions = new HashMap<>();

    void notifyMessageReceived(WsMessage wsMessage) {
        synchronized (receiveListeners) {
            for (WsMessageReceiveListener listener : receiveListeners) {
                listener.messageReceived(wsMessage);
            }
        }
    }

    void addMessageReceiveListener(WsMessageReceiveListener listener) {
        synchronized (receiveListeners) {
            receiveListeners.add(listener);
        }
    }

    void removeMessageReceiveListener(WsMessageReceiveListener listener) {
        synchronized (receiveListeners) {
            receiveListeners.remove(listener);
        }
    }

    void addSession(Session session, WsMessagePushListener listener) {
        synchronized (connectedSessions) {
            connectedSessions.put(session.getId(), new Pair<>(session, listener));
        }
    }

    void removeSession(Session session) {
        synchronized (connectedSessions) {
            connectedSessions.remove(session.getId());
        }
    }

    void notifyMessagePushListeners(WsMessage message) {
        if (message.getSessionId() != null) {
            Pair<Session, WsMessagePushListener> pair;
            synchronized (connectedSessions) {
                pair = connectedSessions.get(message.getSessionId());
            }

            if (pair != null) {
                pair.getV().pushMessage(message, pair.getU());
            } else {
                log(WebFrontendManager.class, INFORM, "Ignoring invalid message because session is not connected: " + message);
            }
        } else {
            log(WebFrontendManager.class, INFORM, "Ignoring invalid message with missing session id: " + message);
        }
    }

    void broadcastMessage(WsMessage message) {
        synchronized (connectedSessions) {
            for (Pair<Session, WsMessagePushListener> pair : connectedSessions.values()) {
                pair.getV().pushMessage(message, pair.getU());
            }
        }
    }
}
