package org.agent.slang.ws;

import org.agent.slang.ws.message.WsMessage;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

import static org.agent.slang.ws.WebFrontendManager.webFrontendManager;
import static org.agent.slang.ws.message.WsMessageFactory.deserializeFromJSON;
import static org.agent.slang.ws.message.WsMessageFactory.serializeToJSON;
import static org.alvarium.logger.Logger.Level.*;
import static org.alvarium.logger.Logger.log;

@ServerEndpoint(value = "/agentSlang-web")
public class WSFrontendService implements WsMessagePushListener {
    public WSFrontendService() { }

    @OnOpen
    public void onOpen(Session session) {
        log(WSFrontendService.class, INFORM, "Session connected: " + session.getId());
        webFrontendManager.addSession(session, this);
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        try {
            WsMessage wsMessage = deserializeFromJSON(message, WsMessage.class);
            wsMessage.setSessionId(session.getId());
            log(WSFrontendService.class, DEBUG, "Received message: " + wsMessage);
            webFrontendManager.notifyMessageReceived(wsMessage);
        } catch (IOException e) {
            log(WSFrontendService.class, CRITICAL, e.getMessage(), e);
        }
    }

    @Override
    public void pushMessage(WsMessage message, Session wsSession) {
        try {
            if (wsSession.isOpen()) {
                wsSession.getBasicRemote().sendText(serializeToJSON(message));
            }
        } catch (IOException e) {
            log(WSFrontendService.class, CRITICAL, "Error pushing remote message: " + e.getMessage(), e);
        }
    }

    @OnClose
    public void onClose(Session session) {
        log(WSFrontendService.class, INFORM, "Session disconnected: " + session.getId());
        webFrontendManager.removeSession(session);
    }
}
