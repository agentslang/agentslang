package org.agent.slang.ws;

import org.agent.slang.ws.message.WsMessage;

interface WsMessageReceiveListener {
    void messageReceived(WsMessage wsMessage);
}
