package org.agent.slang.ws;

import org.agent.slang.ws.message.WsMessage;

import javax.websocket.Session;

public interface WsMessagePushListener {
    void pushMessage(WsMessage message, Session wsSession);
}
