package org.agent.slang.ws;

import org.agent.slang.data.audio.AudioData;
import org.agent.slang.data.audio.PlayerEvent;
import org.agent.slang.util.Mp3Util;
import org.agent.slang.ws.message.WsMessage;
import org.agent.slang.ws.message.WsMessageType;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.container.monitor.MonitorChangeEvent;
import org.alvarium.container.monitor.MonitorChangeListener;
import org.alvarium.utils.FileUtils;
import org.alvarium.utils.LanguageUtils;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.tyrus.server.Server;

import javax.websocket.DeploymentException;
import javax.websocket.server.ServerEndpoint;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.UUID;

import static org.agent.slang.util.PropertyUtils.getFilename;
import static org.agent.slang.util.PropertyUtils.getIntegerProperty;
import static org.agent.slang.ws.WebFrontendManager.webFrontendManager;
import static org.alvarium.container.monitor.SystemMonitorManager.systemMonitorManager;
import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.Level.DEBUG;
import static org.alvarium.logger.Logger.log;

@ConfigureParams(
        mandatoryConfigurationParams = {"networkName", "wsPort", "httpPort", "webPath", "audioCachePath"},
        outputChannels = {"text.data", "player.event.data"},
        outputDataTypes = {StringData.class, PlayerEvent.class},
        inputDataTypes = {StringData.class, AudioData.class}
)

public class WebFrontendSource extends MixedComponent implements WsMessageReceiveListener, MonitorChangeListener {
    private static final String textData = "text.data";
    private static final String playerEventData = "player.event.data";

    private static final String PROPERTY_NETWORK_NAME = "networkName";
    private static final String PROPERTY_WS_PORT = "wsPort";
    private static final String PROPERTY_HTTP_PORT = "httpPort";
    private static final String PROPERTY_WEB_PATH = "webPath";
    private static final String PROPERTY_AUDIO_CACHE_PATH = "audioCachePath";

    private static final String FILENAME_JS_CONFIG = "js/wsConfig.js";
    private static final String AUDIO_CACHE_PATH = "/audioCache";

    private static final String AUDIO_CACHE_PATTERN = "http://%s:%s%s/";

    private Server wsServer;
    private HttpServer httpServer;

    private File audioCache;
    private String webAudioCachePath;

    public WebFrontendSource(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        logProperty(PROPERTY_NETWORK_NAME, listProperties);
        logProperty(PROPERTY_WS_PORT, listProperties);
        logProperty(PROPERTY_HTTP_PORT, listProperties);
        logProperty(PROPERTY_WEB_PATH, listProperties);
        logProperty(PROPERTY_AUDIO_CACHE_PATH, listProperties);

        String networkName = listProperties.getProperty(PROPERTY_NETWORK_NAME);
        Integer wsPort = getIntegerProperty(listProperties.getProperty(PROPERTY_WS_PORT));
        Integer httpPort = getIntegerProperty(listProperties.getProperty(PROPERTY_HTTP_PORT));

        File webPath = getFilename(listProperties.getProperty(PROPERTY_WEB_PATH));
        FileUtils.checkReadableFile(webPath, true);

        audioCache = getFilename(listProperties.getProperty(PROPERTY_AUDIO_CACHE_PATH));
        FileUtils.createCacheDir(this, audioCache);

        // Extreme debug for Tyrus, uses java.util.Logger
        //        Logger.getLogger("").setLevel(Level.FINEST);
        //        for (Handler handler : Logger.getLogger("").getHandlers()) {
        //            handler.setLevel(Level.FINEST);
        //        }

        if (wsPort != null) {
            systemMonitorManager.addListener(this);

            webFrontendManager.addMessageReceiveListener(this);

            wsServer = new Server(networkName, wsPort, null, null, WSFrontendService.class);
            try {
                wsServer.start();
            } catch (DeploymentException e) {
                log(WSFrontendService.class, CRITICAL, "Deployment exception", e);
            }
            createJsConfigFile(webPath, networkName, wsPort, getWsResourcePath());
        }

        if (httpPort != null) {
            httpServer = new HttpServer();
            NetworkListener listener = new NetworkListener("audioCache", networkName, httpPort);
            httpServer.addListener(listener);

            httpServer.getServerConfiguration().addHttpHandler(new StaticHttpHandler(webPath.getAbsolutePath()), "/");
            httpServer.getServerConfiguration()
                      .addHttpHandler(new StaticHttpHandler(audioCache.getAbsolutePath()), AUDIO_CACHE_PATH);

            try {
                httpServer.start();
            } catch (IOException e) {
                log(WSFrontendService.class, CRITICAL, "Deployment exception", e);
            }

            computeAudioCachePath(networkName, httpPort);
        }
    }

    @Override
    public void messageReceived(WsMessage wsMessage) {
        switch (wsMessage.getType()) {
            case message:
                int langCode = LanguageUtils.getLanguageCodeByLocale(Locale.US);
                publishData(textData, new StringData(getMessageID(), wsMessage.getSessionId(),
                                                     wsMessage.getMessage(), langCode));
                break;
            case system:
                if (wsMessage.getMessage().equals("ready")) {
                    sendWelcomeMessage(wsMessage.getSessionId());
                } else if (wsMessage.getMessage().startsWith("player-")) {
                    log(this, DEBUG, "Player event: " + wsMessage.getMessage());
                    publishPlayerEvent(wsMessage.getSessionId(), wsMessage.getMessage());
                }
                break;
        }
    }

    @Override
    protected void handleData(GenericData data) {
        if (data instanceof AudioData) {
            String sessionId = ((AudioData) data).getSessionId();
            String filename = getVoicePath(sessionId, saveToFile(sessionId, data.getId(), ((AudioData) data)));
            String text = ((AudioData) data).getTextTranscription();
            WsMessage wsMessage = new WsMessage(WsMessageType.message, text, filename);
            wsMessage.setSessionId(sessionId);
            webFrontendManager.notifyMessagePushListeners(wsMessage);
        } else {
            WsMessage wsMessage = new WsMessage(WsMessageType.message, ((StringData) data).getData(), null);
            wsMessage.setSessionId(((StringData) data).getSessionId());
            webFrontendManager.notifyMessagePushListeners(wsMessage);
        }
    }

    @Override
    public void containerEventFired(MonitorChangeEvent monitorChangeEvent) {
        if (monitorChangeEvent.getEventType() != MonitorChangeEvent.EventType.act) {
            broadcastDebugMessage("Container id = " + monitorChangeEvent.getContainerId()
                                          + " fired an event = " + monitorChangeEvent.getEventType());
        }
    }

    @Override
    public void componentEventFired(MonitorChangeEvent monitorChangeEvent) {
        if (monitorChangeEvent.getEventType() != MonitorChangeEvent.EventType.act) {
            broadcastDebugMessage("Component id = " + monitorChangeEvent.getContainerId() + "::" +
                                          monitorChangeEvent.getComponentId() + " fired an event = " +
                                          monitorChangeEvent.getEventType());
        }
    }

    private void sendWelcomeMessage(String sessionId) {
        String dummyMessage = "Welcome traveller. My name is Alex, which is short from Agent Slang ... but anyway, " +
                "I will be your guide today. You can ask me some questions and I will try my best to answer them.";
        int langCode = LanguageUtils.getLanguageCodeByLocale(Locale.US);
        StringData data = new StringData(getMessageID(), sessionId, dummyMessage, langCode);
        publishData(textData, data);
    }

    private void broadcastDebugMessage(String message) {
        WsMessage wsMessage = new WsMessage(WsMessageType.debug, message, null);
        webFrontendManager.broadcastMessage(wsMessage);
    }

    private String getMessageID() {
        return UUID.randomUUID().toString();
    }

    private void logProperty(String propertyName, ListProperties listProperties) {
        log(WebFrontendSource.class, DEBUG, propertyName + " = " + listProperties.getProperty(propertyName));
    }

    private void createJsConfigFile(File webPath, String networkName, int wsPort, String resourcePath) {
        File jsConfigFile = new File(webPath, FILENAME_JS_CONFIG);
        try (PrintWriter out = new PrintWriter(new FileWriter(jsConfigFile))) {
            out.printf("wsUrl = \"ws://%s:%s/%s\";\n", networkName, wsPort, resourcePath);
        } catch (IOException e) {
            log(WebFrontendSource.class, CRITICAL, "Unable to write config file: " + jsConfigFile.getAbsolutePath(), e);
        }
    }

    private static String getWsResourcePath() {
        ServerEndpoint serverAnnotation = WSFrontendService.class.getAnnotation(ServerEndpoint.class);
        if (serverAnnotation != null) {
            return serverAnnotation.value().replaceAll("/", "").trim();
        } else {
            log(WebFrontendSource.class, CRITICAL, "Unable to retrieve the resource annotation for class ...");
            return "";
        }
    }

    private void computeAudioCachePath(String networkName, int httpPort) {
        webAudioCachePath = String.format(AUDIO_CACHE_PATTERN, networkName, httpPort, AUDIO_CACHE_PATH);
    }

    private String getVoicePath(String sessionId, String filename) {
        return webAudioCachePath + sessionId + "/" + filename;
    }

    private String saveToFile(String sessionId, String fileID, AudioData data) {
        File sessionPath = new File(audioCache, sessionId);
        FileUtils.createCacheDir(this, sessionPath);

        File audioFileData = new File(sessionPath, "audio-" + fileID + ".mp3");
        try {
            Mp3Util.saveMp3File(audioFileData, data.getAudioStream());
            return audioFileData.getName();
        } catch (IOException e) {
            log(this, CRITICAL, "Invalid save path = " + audioFileData.getAbsolutePath(), e);
            return null;
        }
    }

    private void publishPlayerEvent(String sessionId, String playerEvent) {
        byte event = -1;

        switch (playerEvent) {
            case "player-play":
                event = PlayerEvent.EVENT_START;
                break;
            case "player-pause":
                event = PlayerEvent.EVENT_PAUSE;
                break;
            case "player-ended":
                event = PlayerEvent.EVENT_STOP;
                break;
        }

        publishData(playerEventData, new PlayerEvent(getMessageID(), sessionId, event));
    }

    @Override
    public void close() {
        super.close();
        systemMonitorManager.removeListener(this);
        webFrontendManager.removeMessageReceiveListener(this);

        if (wsServer != null) {
            wsServer.stop();
        }

        if (httpServer != null) {
            httpServer.shutdownNow();
        }
    }
}
