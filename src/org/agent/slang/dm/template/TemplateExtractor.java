/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.dm.template;

import org.agent.slang.data.annotation.GenericTextAnnotation;
import org.agent.slang.data.template.TemplateData;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.syn.n.bad.dictionary.Dictionary;
import org.syn.n.bad.dictionary.DictionaryException;
import org.syn.n.bad.pattern.Matcher;
import org.syn.n.bad.pattern.PatternMatcher;
import org.syn.n.bad.pattern.TemplateMatchResult;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.agent.slang.util.PropertyUtils.getFilename;
import static org.agent.slang.util.XmlUtils.deserializeFromXml;
import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.log;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 12/31/12
 */
@ConfigureParams(mandatoryConfigurationParams = {"dialogueModel", "dictionaryConfig"},
        outputChannels = {"templateExc.dialogue.data"},
        outputDataTypes = {TemplateData.class},
        inputDataTypes = GenericTextAnnotation.class)
public class TemplateExtractor extends MixedComponent {
    private static final String dialogueOutFeed = "templateExc.dialogue.data";

    private static final String PROP_DIALOGUE = "dialogueModel";
    private static final String PROP_DICTIONARY = "dictionaryConfig";

    private Matcher matcherDialogue;

    public TemplateExtractor(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        try {
            Dictionary.setupDictionary(getFilename(listProperties.getProperty(PROP_DICTIONARY)));
        } catch (DictionaryException e) {
            throw new IllegalStateException(e);
        }
        matcherDialogue = new Matcher();
        loadDialogueMatcher(getFilename(listProperties.getProperty(PROP_DIALOGUE)), matcherDialogue);
    }

    private void loadDialogueMatcher(File path, Matcher matcher) {
        if (path != null && path.canRead()) {
            try {
                FileReader fileReader = new FileReader(path);
                DialogueUnitList commandTemplateList = deserializeFromXml(DialogueUnitList.class, fileReader);
                fileReader.close();

                for (DialogueUnit template : commandTemplateList) {
                    for (String pattern : template.getDialoguePatterns()) {
                        matcher.addMatcher(new PatternMatcher(template.getId(), pattern));
                    }

                }
            } catch (IOException e) {
                log(this, CRITICAL, "Dialogue template file not found: " + path, e);
            }
        }
    }

    protected void handleData(GenericData data) {
        TemplateData result = processText((GenericTextAnnotation) data, matcherDialogue);
        publishData(dialogueOutFeed, result);
    }

    private TemplateData processText(GenericTextAnnotation annotation, Matcher matcher) {
        TemplateMatchResult templateMatchResult = matcher.match(annotation);
        TemplateData result = new TemplateData(annotation.getId(), annotation.getSessionId(), annotation.getLanguage());
        result.updateVariables(templateMatchResult.getExtractedVars());
        result.addTemplateIds(templateMatchResult.getTemplateIDs());
        return result;
    }
}
