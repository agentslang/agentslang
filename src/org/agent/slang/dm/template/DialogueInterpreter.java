/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.dm.template;

import org.agent.slang.data.template.TemplateData;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.utils.FileUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.agent.slang.util.XmlUtils.deserializeFromXml;
import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.log;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 1/2/13
 */
@ConfigureParams(mandatoryConfigurationParams = "dialogueModel",
        outputChannels = "dialogue.data", outputDataTypes = StringData.class,
        inputDataTypes = TemplateData.class)
public class DialogueInterpreter extends MixedComponent {
    private static final String feedbackChannel = "dialogue.data";
    private static final String PROP_DIALOGUE = "dialogueModel";
    private Map<String, DialogueUnit> dialogueTemplateMap;
    private DialogueUnit defaultDialogueUnit;

    public DialogueInterpreter(ComponentConfig config) {
        super(config);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        File templateFile = new File(listProperties.getProperty(PROP_DIALOGUE));
        FileUtils.checkReadableFile(templateFile, true);

        dialogueTemplateMap = new HashMap<>();
        loadDialogueTemplates(templateFile);
    }

    private void loadDialogueTemplates(File path) {
        try {
            FileReader fileReader = new FileReader(path);
            DialogueUnitList commandTemplateList = deserializeFromXml(DialogueUnitList.class, fileReader);
            fileReader.close();

            for (DialogueUnit template : commandTemplateList) {
                if (DialogueUnit.DEFAULT.equals(template.getId())) {
                    defaultDialogueUnit = template;
                } else {
                    dialogueTemplateMap.put(template.getId(), template);
                }
            }
        } catch (IOException e) {
            log(this, CRITICAL, "Dialogue template file not found: " + path, e);
        }
    }

    protected void handleData(GenericData data) {
        int languageCode = ((TemplateData) data).getLanguage();
        Map<String, String> variableMap = ((TemplateData) data).getExtractedVars();

        String reply = null;

        if (!((TemplateData) data).isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (String id : ((TemplateData) data).getTemplateIDs()) {
                DialogueUnit template = dialogueTemplateMap.get(id);
                sb.append(prepareReply(template, variableMap));
            }

            reply = sb.toString().trim();

        }

        if ((reply == null || reply.length() == 0) && defaultDialogueUnit != null) {
            reply = prepareReply(defaultDialogueUnit, variableMap).trim();
        }

        if (reply == null || reply.length() == 0) {
            String sessionId = ((TemplateData) data).getSessionId();
            publishData(feedbackChannel, new StringData(data.getId(), sessionId, reply, languageCode));
        }
    }

    private String prepareReply(DialogueUnit template, Map<String, String> variableMap) {
        StringBuilder sb = new StringBuilder();

        if (template != null && template.getResponsePatterns() != null && !template.getResponsePatterns().isEmpty()) {
            for (String templateReply : template.getResponsePatterns()) {
                sb.append(prepareTemplate(templateReply, variableMap)).append(" ");
            }
        }

        return sb.toString();
    }

    private String prepareTemplate(String template, Map<String, String> variableMap) {
        // todo; multiple variables
        // todo; variables can be terminated by a space or any other punctuation
        if (template.contains("$")) {
            int index = template.indexOf("$");
            int afterIndex = template.indexOf(" ", index + 1);
            if (afterIndex <= 0) {
                afterIndex = template.length();
            }

            String varName = template.substring(index + 1, afterIndex);
            if (variableMap.containsKey(varName)) {
                return template.substring(0, index) + " " + variableMap.get(varName) + " " + template
                        .substring(afterIndex);
            } else {
                return "";
            }
        } else {
            return template;
        }
    }
}
