/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.dm.template;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 1/5/13
 */

@XmlRootElement(name = "dialogue-units")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class DialogueUnitList implements Iterable<DialogueUnit> {
    @XmlElement(name = "du", required = false)
    private List<DialogueUnit> dialogueUnitList = new LinkedList<>();

    public DialogueUnitList() {
    }

    public List<DialogueUnit> getDialogueUnitList() {
        return dialogueUnitList;
    }

    public void setDialogueUnitList(List<DialogueUnit> dialogueUnitList) {
        this.dialogueUnitList = dialogueUnitList;
    }

    @Override
    public Iterator<DialogueUnit> iterator() {
        return dialogueUnitList.iterator();
    }

    @Override
    public String toString() {
        return "DialogueUnitList{" +
                "dialogueUnitList=" + dialogueUnitList +
                '}';
    }
}
