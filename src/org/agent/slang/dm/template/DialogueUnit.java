/*
 * Copyright (c) Ovidiu Serban, ovidiu@roboslang.org
 *               web:http://ovidiu.roboslang.org/
 * All Rights Reserved. Use is subject to license terms.
 *
 * This file is part of AgentSlang Project (http://agent.roboslang.org/).
 *
 * AgentSlang is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License and CECILL-B.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The CECILL-B license file should be a part of this project. If not,
 * it could be obtained at  <http://www.cecill.info/>.
 *
 * The usage of this project makes mandatory the authors citation in
 * any scientific publication or technical reports. For websites or
 * research projects the AgentSlang website and logo needs to be linked
 * in a visible area.
 */

package org.agent.slang.dm.template;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author Ovidiu Serban, ovidiu@roboslang.org
 * @version 1, 1/5/13
 */

@XmlRootElement(name = "du")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class DialogueUnit {
    public static final String DEFAULT = "default";

    @XmlAttribute(name = "id", required = true)
    private String id;

    @XmlElementWrapper(name = "patterns", required = false)
    @XmlElement(name = "pattern", required = false)
    private List<String> dialoguePatterns;

    @XmlElementWrapper(name = "responses", required = false)
    @XmlElement(name = "response", required = false)
    private List<String> responsePatterns;

    public DialogueUnit() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getDialoguePatterns() {
        return dialoguePatterns;
    }

    public void setDialoguePatterns(List<String> dialoguePatterns) {
        this.dialoguePatterns = dialoguePatterns;
    }

    public List<String> getResponsePatterns() {
        return responsePatterns;
    }

    public void setResponsePatterns(List<String> responsePatterns) {
        this.responsePatterns = responsePatterns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DialogueUnit that = (DialogueUnit) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "DialogueUnit{" +
                "id='" + id + '\'' +
                ", dialoguePatterns=" + dialoguePatterns +
                ", responsePatterns=" + responsePatterns +
                '}';
    }
}
