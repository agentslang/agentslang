package org.agent.slang.dm.search;

import org.agent.slang.data.annotation.GenericTextAnnotation;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.MixedComponent;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.SessionDependentData;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.utils.LanguageUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;

@ConfigureParams(mandatoryConfigurationParams = {"elasticUri", "searchField", "replyField"},
        outputChannels = {"elastic.reply.data"},
        outputDataTypes = {StringData.class},
        inputDataTypes = {GenericTextAnnotation.class, StringData.class})

public class ElasticSearch extends MixedComponent {
    private static final String PROPERTY_URI = "elasticUri";
    private static final String PROPERTY_SEARCH_FIELD = "searchField";
    private static final String PROPERTY_REPLY_FIELD = "replyField";

    private static final int DEFAULT_PORT = 9300;

    private static final String TOPIC_OUT = "elastic.reply.data";

    private Client client;
    private String index;
    private String type;
    private String queryField;
    private String replyField;

    public ElasticSearch(ComponentConfig componentConfig) {
        super(componentConfig);
    }

    @Override
    protected void setupComponent(ListProperties listProperties) {
        URI uri = URI.create(listProperties.getProperty(PROPERTY_URI));

        if (!"elasticsearch".equalsIgnoreCase(uri.getAuthority())) {
            throw new IllegalArgumentException("Invalid URI, it has to start with elasticsearch protocol");
        }

        try {
            InetAddress address = InetAddress.getByName(uri.getHost());
            int port = uri.getPort() == -1 ? DEFAULT_PORT : uri.getPort();

            client = TransportClient.builder().build()
                                    .addTransportAddress(new InetSocketTransportAddress(address, port));

            if (uri.getPath() != null && !uri.getPath().isEmpty()) {
                String[] parts = uri.getPath().split("/");
                index = "nothing";

                if (parts.length > 0) {
                    index = parts[0];
                }

                if (parts.length > 1) {
                    type = parts[1];
                } else {
                    type = index;
                }
            }

            queryField = listProperties.getProperty(PROPERTY_SEARCH_FIELD);
            replyField = listProperties.getProperty(PROPERTY_REPLY_FIELD);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    protected void handleData(GenericData genericData) {
        String searchText = "";
        int language = LanguageUtils.IDX_NONE;
        if (genericData instanceof GenericTextAnnotation) {
            searchText = ((GenericTextAnnotation) genericData).getTranscription();
            language = ((GenericTextAnnotation) genericData).getLanguage();
        } else if (genericData instanceof StringData) {
            searchText = ((StringData) genericData).getData();
            language = ((StringData) genericData).getLanguage();
        }

        SearchResponse response = client.prepareSearch(index).setTypes(type).addField(replyField)
                                        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                                        .setQuery(QueryBuilders.matchQuery(queryField, searchText))
                                        .setFrom(0).setSize(1).execute().actionGet();

        if (response.getHits().getTotalHits() > 0) {
            String sessionId = ((SessionDependentData) genericData).getSessionId();
            String reply = response.getHits().getAt(0).field(replyField).getValue();

            publishData(TOPIC_OUT, new StringData(genericData.getId(), sessionId, reply, language));
        }
    }

    @Override
    public void close() {
        super.close();

        if (client != null) {
            client.close();
        }
    }
}
