package org.syn.n.bad.dictionary.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "extension")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class DictionaryExtension {
    @XmlAttribute(name = "name", required = true)
    private String name;
    @XmlAttribute(name = "prefix", required = true)
    private String prefix;
    @XmlAttribute(name = "config", required = true)
    private String config;

    public DictionaryExtension() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    @Override
    public String toString() {
        return "DictionaryExtension{" +
                "name='" + name + '\'' +
                ", prefix='" + prefix + '\'' +
                ", config='" + config + '\'' +
                '}';
    }
}
