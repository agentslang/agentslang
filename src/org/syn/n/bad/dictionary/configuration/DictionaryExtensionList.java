package org.syn.n.bad.dictionary.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "extensions")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class DictionaryExtensionList implements Iterable<DictionaryExtension> {
    @XmlElement(name = "extension")
    private List<DictionaryExtension> extensions = new LinkedList<>();

    public DictionaryExtensionList() {}

    public List<DictionaryExtension> getExtensions() {
        return extensions;
    }

    public void setExtensions(List<DictionaryExtension> extensions) {
        this.extensions = extensions;
    }

    @Override
    public Iterator<DictionaryExtension> iterator() {
        return extensions.iterator();
    }

    @Override
    public String toString() {
        return "DictionaryExtensionList{" +
                "extensions=" + extensions +
                '}';
    }
}
